class Post < ActiveRecord::Base
  acts_as_taggable
  ## Associations, delegates
  belongs_to :user
  has_many :comments, dependent: :destroy

  ## Validations
  validates :title, :text, :published_at, presence: true

  paginates_per 5

  scope :by_published, -> { where(published: true) }
end
