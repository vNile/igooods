class CommentDecorator < Draper::Decorator
  delegate_all

  def author?(user)
    (user.id == object.user.id) && (object.created_at + 15.minutes > Time.now)
  end
end
