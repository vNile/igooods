class PostDecorator < Draper::Decorator
  delegate_all

  def date_creation
    object.created_at.strftime('%d/%m/%Y') if object.created_at.present?
  end
end
