#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require bootstrap-sprockets
#= require bootstrap-datepicker
#= require best_in_place
#= require best_in_place.purr
#= require jquery.purr
#= require_tree .

$(document).ready ->
  $('.datepicker').datepicker format: 'dd/mm/yyyy'

  $('.slider').slick
    dots: false
    infinite: true
    speed: 500
    fade: true
    cssEase: 'linear'
    prevArrow: '.arrow_left'
    nextArrow: '.arrow_right'

jQuery ->
  $('.best_in_place').best_in_place()
