class CommentsController < ApplicationController
  before_action :find_post, only: [:create, :destroy, :update, :edit, :find_comment]
  before_action :find_comment, only: [:update, :destroy]
  respond_to :html, :json

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(comment_params)
    redirect_to post_path(@post)
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @comment.update_attributes(comment_params)
        format.json { respond_with_bip(@comment) }
      else
        format.json { respond_with_bip(@comment) }
      end
    end
  end

  def destroy
    @comment.destroy
    redirect_to post_path(@post)
  end

  private

  def comment_params
    params[:comment].merge!(user_id: current_user.id)
    params.require(:comment).permit(:body, :user_id, :published_at)
  end

  def find_post
    @post = Post.find(params[:post_id])
  end

  def find_comment
    @comment = @post.comments.find(params[:id])
  end
end
