class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_post, only: [:show, :destroy, :edit, :update]

  def index
    @posts = if params[:tag]
      Post.by_published.page(params[:page]).order('created_at DESC').tagged_with(params[:tag])
    else
      Post.by_published.page(params[:page]).order('created_at DESC')
    end
  end

  def new
    @post = Post.new
  end

  def show
  end

  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id
    @post.published_at = Time.now
    if @post.save
      flash[:success] = 'Пост успешно создан'
      redirect_to my_posts_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      redirect_to(@post)
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    flash[:alert] = 'Пост успешно удален'
    redirect_to my_posts_path
  end

  def my_posts
    @posts = current_user.posts.page(params[:page]).order('created_at DESC')
  end

  def tag_cloud
    @tags = Post.tag_counts_on(:tags)
  end

  private

  def post_params
    params[:post].permit(:title, :text, :published_at, :tag_list)
  end

  def find_post
    @post = Post.find(params[:id])
  end
end
