class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :user
      t.string :title
      t.text :text
      t.datetime :published_at
      t.boolean :published, default: true

      t.timestamps null: false
    end

    add_index :posts, :user_id
    add_index :posts, :published_at
    add_index :posts, :published
  end
end
